/**
 * "Word of the Day" by Goga Tamás, in October 2015
 *
 * INFO:
 * Copies the word of the day to the day from dictionary.com to a temporary file.
 * If the system has gedit installed the file will be opened in said text editor.
 * It was tested on a 64 bit Linux Mint 17.2 Cinnamon Edition.
 * RegExes will probably break whenever dictionary.com updates their HTML layout.
   Try fixing it by updating them in "wotd.regex". If that isn't enough, this
   program needs to be tweaked as well.
 *
 * LICENSE:
 * No rights are reserved, the software is provided "as is" without any sort of
   guarantees about its quality. As such, anyone who happens upon this can use it
   at his or her own discretion.
 */



/* Program info */
#define PROG_NAM			"Word of the Day"
#define PROG_VER			"1.1_4"

/* Here for ease of redefinition */
#define SITEURL			"http://www.dictionary.com/wordoftheday/"
#define REGXFILE		"wotd.regex"

#define DESCR_STREAM	PROG_NAM " " PROG_VER ":\nCopies the definition of the Word of the Day from dictionary.com to " << DESTFILE
#define USAGE_STREAM	"Usage: " << argv[0] << " [-h or --help]"

/* Just in case -- I can be dumb "sometimes"--; undefined, so it doesn't muck up anything */
#define REGEX_WORD		"(Definitions for <strong>[^<]+</strong>)"
#define REGEX_DEFN		"<li class=\"\\w+\">(.+)</li>"
#undef REGEX_WORD
#undef REGEX_DEFN


#include "downloader.hpp"

#include "os/os.hpp"

/* Other headers */
#include <iostream>
#include <fstream>
#include <sstream>

#include <cstring>
#include <vector>

#include <regex>

#include <exception>


/* Magic variables */
constexpr int MAX_LINE_SIZE = 60;


/* Downloads the website */
bool download(const char* url, std::string& str) {
	Downloader dl;

	if (!dl.isInit()) {
		std::fputs("ERROR: Downloader could NOT be initialized", stderr);
		return false;
	}

	std::cout << "Downloader initialized\n"
			  << "Starting to download\n  > from: " << url << "\n  > to:   " << DESTFILE << '\n';

	if (dl.download(url, str)) {
		std::puts("Download finished");
	} else {
		std::puts("Download FAILED");
		return false;
	}

	return true;
}


/* Removes HTML tags from a string */
std::string removeHtml(const std::string& src) {
	std::vector<char> result(src.size() + 1, static_cast<char>(0));
	size_t len = 0;

	unsigned int tagLayer = 0;

	for (auto it = src.cbegin(); it != src.cend(); ++it) {
		if (*it == '<')
			++tagLayer;
		else if (*it == '>')
			--tagLayer;
		else if (tagLayer == 0)
			result[len++] = *it;
	}

	return std::string(result.data());
}


/* Ensure that the maximum line length is respected */
std::string enforceMaxLineLength(const std::string& str) {
	std::string ret;
	ret.reserve(str.size());

	std::istringstream iss(str);
	std::string line;

	while (std::getline(iss, line)) {
		if (line.size() > static_cast<std::string::size_type>(::MAX_LINE_SIZE)) {
			int i;

			// find the first whitespace and add a newline char
			for (i = ::MAX_LINE_SIZE; i >= 0; --i) {
				if (line[i] == ' ' || line[i] == '\t') {
					line[i] = '\n';
					break;
				}
			}

			// no whitespace chars => insert newline
			if (i < 0) line.insert(::MAX_LINE_SIZE, 1, '\n');
		}

		ret += line + "\n";
	}

	return ret;
}


/* Get RegExes from a file; RELATIVE PATH */
std::vector<std::regex> getRegexes(const char* from) {
	std::vector<std::regex> ret;

	auto path = getProcessDirectory();
	path += from;

	std::ifstream file(path);
	if (file.is_open()) {
		std::string line;
		const std::string endMarker = "#THE_END";
		bool foundEndMarker = false;
		unsigned int timesLooped = 0;

		try {
			for (;std::getline(file, line); ++timesLooped) {
				if (line == endMarker) {
					foundEndMarker = true;
					break;
				}

				ret.push_back(std::regex(line.c_str()));
			}
		} catch (const std::exception& except) {
			std::fprintf(stderr, "ERROR: %s\n", except.what());
			ret.clear();
		}

		if (!foundEndMarker) {
			std::fprintf(stderr, "ERROR: %s doesn't contain end marker %s\n", path.c_str(), endMarker.c_str());
			ret.clear();
		}

		if (timesLooped == 0) {
			std::fprintf(stderr, "ERROR: 0 RegExes found in %s\n", path.c_str());
		}

		file.close();
	} else {
		std::fprintf(stderr, "ERROR: Unable to find %s\n", path.c_str());
	}

	return ret;
}


/* Parses the string and saves it to a file */
std::string parse(const std::string& str) {
	auto regexes = getRegexes(REGXFILE);
	if (regexes.empty())
		return std::string();

	std::smatch match;
	std::string result = "";
	std::stringstream ss;

	size_t timesWritten = 0;

	try {
		for (auto rex = regexes.cbegin(); rex != regexes.cend(); ++rex) {
			for (auto it = str.cbegin(); std::regex_search(it, str.cend(), match, *rex); it += match.position() + match.length()) {
				ss << (timesWritten == 0 ? "" : "\n") << match.str(1);
				++timesWritten;
			}
		}
		ss.flush();
	} catch (const std::exception& except) {
		std::fprintf(stderr, "ERROR: %s\n", except.what());
		return std::string();
	}

	return enforceMaxLineLength(removeHtml(ss.str()));
}


bool saveToFile(const std::string& str, const char* path) {
	std::ofstream file(path);
	if (file.is_open()) {
		file << str;
		file.close();
	} else {
		std::fprintf(stderr, "ERROR: Unable to open %s\n", path);
		return false;
	}

	return true;
}


/* Burt Macklin, FBI. Runs the whole operation. */
void run() {
	std::string website;
	if (!download(SITEURL, website))
		return;

	std::string wotd = parse(website);
	if (wotd.empty())
		return;

	if (!saveToFile(wotd, DESTFILE)) {
		std::printf("\n%s\n", wotd.c_str());
	} else {
		std::printf("Saved to %s\n", DESTFILE);
		openInTextEditor();
	}
}


/* It's main, ya dummy! */
int main(int argc, char** argv) {
	switch (argc) {

	/* run the thing */
	case 1:
		try {
			run();
		} catch(const std::exception& except) {
			std::fprintf(stderr, "%s\n", except.what());
		}
		break;

	/* user might have asked for help */
	case 2:
		if (std::strcmp(argv[1], "-h") == 0 || std::strcmp(argv[1], "--help") == 0) {
			std::cout << DESCR_STREAM << '\n' << USAGE_STREAM << std::endl;
			break;
		}

	/* bad arguments */
	default:
		std::cout << "Unknown arguments; " << USAGE_STREAM << std::endl;
		break;

	} // switch
}



/** VERSION HISTORY

	Version 1.1_4:
		- Made 1.1_3 work on Linux

	Version 1.1_3:
		- Reorganised files a bit for better support of os specific code
		- Added windows implementation for os specific code
		- Added windows project files and build configuration for visual studio

	Version 1.1_2:
		- Refactoring for portability.
		- Switched from variable-length C-style arrays to vectors.

	Version 1.1_1:
		- Constexpressions are awesome!

	Version 1.1:
		- RegExes can now be specified in "wotd.regex" next to the executable. Instructions within.
		- Optimization. Lots of it.
		- Error messages. They exist now.

	Version 1.0:
		- Copies the definition from http://www.dictionary.com/wordoftheday/ into a temporary file.

*/
