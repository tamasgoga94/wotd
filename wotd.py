url = "http://www.dictionary.com/wordoftheday/"
dest = "/tmp/wotd.html"
regexfile = "wotd.regex"
regexfile_end = "#THE_END"

import requests

print "GET " + url;
r = requests.get(url)

if r.status_code != 200:
	print r.status_code, "Aborting!"
	exit(1);
else:
	print r.status_code, "OK!", len(r.content), "characters"

html = r.content

import re
regexes = [];

with open(regexfile) as f:
	lines = f.readlines()
	for line in lines:
		str = line.strip()
		if str == regexfile_end:
			break
		regexes.append(re.compile(str))

print "loaded", len(regexes), "regex(es) from", regexfile


regex_striphtml = re.compile('<.*?>')
results = []

for regex in regexes:
	match = regex.search(html).group(1)
	clean = re.sub(regex_striphtml, '', match)
	results.append(clean)

print "writing", len(results), "results to", dest

with open(dest, "w") as f:
	for result in results:
		f.write(result + "\n")

import os
os.system("gedit " + dest)





