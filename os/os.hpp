#ifndef GG_DICTWOTD_OS_LINUX_LINUX_HPP
#define GG_DICTWOTD_OS_LINUX_LINUX_HPP

#define DESTFILE (getTemporaryFilePath().c_str())

#include <string>


/* Get the working directory of our process */
std::string getProcessDirectory();

/* Get a path to a temporary file */
std::string getTemporaryFilePath();

/* Open the output in text editor */
void openInTextEditor();


#endif // GG_DICTWOTD_OS_LINUX_LINUX_HPP
