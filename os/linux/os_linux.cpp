#include "../os.hpp"
#include <unistd.h>
#include <cstdio>
#include <sstream>
#include <algorithm>


/* Heavily inspired from: http://stackoverflow.com/questions/143174/how-do-i-get-the-directory-that-a-program-is-running-from
 * Thank you, Mike, you are a hero!
 */
std::string getProcessDirectory() {
	constexpr long len = 255;
	char tmpstr[len];
	char buf[len];

	if (std::sprintf(tmpstr, "/proc/%d/exe", getpid()) < 0) {
		std::fputs("ERROR: Unable to get the process's full name", stderr);
		return std::string();
	}

	// get the name of the process
	// for more info: http://pubs.opengroup.org/onlinepubs/009695399/functions/readlink.html
	ssize_t bytes = std::min(readlink(tmpstr, buf, len), len-1);

	if (bytes < 0) {
		std::fputs("ERROR: Unable to get the program's original full path", stderr);
		return std::string();
	}

	// remove the name of the program (ending up with only the full path to it)
	for (; bytes > 0 && buf[bytes-1] != '/'; --bytes) {
		continue;
	}
	buf[bytes] = '\0';

	return std::string(buf);
}

/* Run and get the output of a system command
 * Inspiration: https://stackoverflow.com/a/646254/4863960
 */
static std::string getSysCommandOutput(const char* cmd, const char* error) {
	using namespace std;

	FILE *pipe;
	char line[1035];
	std::stringstream output;

	// Open the command for reading
	pipe = popen(cmd, "r");
	if (pipe == NULL)
		throw std::runtime_error(error);

	// Read the output a line at a time
	while (fgets(line, sizeof(line), pipe) != NULL)
		output << line;

	pclose(pipe);

	// remove newlines
	auto ret = output.str();
	ret.erase(remove(ret.begin(), ret.end(), '\n'), ret.end());

	return ret;
}

/* Open the output in text editor */
void openInTextEditor() {
	auto command = getSysCommandOutput("xdg-mime query default text/plain", "Failed to get file association.");
	command.erase(command.find_last_of('.'));
	command += std::string(" ") + DESTFILE + " &";
	system(command.c_str());
}

std::string getTemporaryFilePath() {
	return "/tmp/wotd.html";
}
