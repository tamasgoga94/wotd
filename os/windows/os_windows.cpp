
#include <string>

// non unicode windows is easier to use
// since rest of the project uses std::string instead of std::wstring, this is easier to work with right now
#undef UNICODE
#include <Windows.h>
#include <Shlwapi.h>

/* get the directory of the executable process */
std::string getProcessDirectory(){
	// return handle to executable module of the current process
	HMODULE hModule = GetModuleHandleW(nullptr);
	CHAR path[MAX_PATH];
	// get the filename of the module
	GetModuleFileNameA(hModule, path, MAX_PATH);

	std::string directory, filename = static_cast<std::string>(path);
	const size_t last_slash_idx = filename.rfind('\\');
	if (std::string::npos != last_slash_idx)
	{
		directory = filename.substr(0, last_slash_idx);
	}
	return directory + "\\";
}

std::string temporaryFile = "";
std::string shortFileName = "wotd.html";

/* get path to the temporary file where the result is written */
std::string getTemporaryFilePath()
{
	if (temporaryFile != "")
		return temporaryFile;
	CHAR directory[MAX_PATH];
	DWORD dwRetVal = 0;
	UINT uRetVal = 0;
	dwRetVal = GetTempPathA(MAX_PATH, directory);
	if (dwRetVal > MAX_PATH || (dwRetVal == 0))
	{
		throw std::runtime_error("Failed to get a temporary folder.");
	}
	return temporaryFile = static_cast<std::string>(directory) + shortFileName;
}

/* open a file with the default associated application */
static void openInDefaultApplication(const std::string& filename)
{
	ShellExecute(0, 0, getTemporaryFilePath().c_str(), 0, 0, SW_SHOW);
}

/* open a file with the default associated application of another file */
static void openInAssociatedType(const std::string& filename, const std::string& file_extension)
{
	CHAR szBuf[1000];
	DWORD cbBufSize = sizeof(szBuf);
	HRESULT hr = AssocQueryString(0, ASSOCSTR_EXECUTABLE,
		file_extension.c_str(), NULL, szBuf, &cbBufSize);
	if (FAILED(hr))
	{
		throw std::runtime_error("Failed to get file association.");
	}
	
	std::string command_line = szBuf;
	command_line += " ";
	command_line += filename;

	CHAR command_line_cstr[2000];
	strcpy_s(command_line_cstr, command_line.c_str());
	
	STARTUPINFO startup_info;
	PROCESS_INFORMATION process_information;

	ZeroMemory(&startup_info, sizeof(startup_info));
	startup_info.cb = sizeof(startup_info);
	ZeroMemory(&process_information, sizeof(process_information));

	BOOL result = CreateProcess(NULL, command_line_cstr, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &startup_info, &process_information);
	if (result == FALSE)
	{
		throw std::runtime_error("Failed to create process.");
	}
}

/* open a file with the default text editor */
static void openInTextEditor(const std::string& filename) {
	openInAssociatedType(getTemporaryFilePath(), ".txt");
}

/* open the output file with text editor */
void openInTextEditor(){
	openInTextEditor(getTemporaryFilePath());
}
