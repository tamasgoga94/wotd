#ifndef GG_DICTWOTD_DOWNLOADER_HPP
#define GG_DICTWOTD_DOWNLOADER_HPP

#include <string>


// forward declaration
typedef void CURL;


/* Uses cURL to download a website */
class Downloader {
public:
	/* C-tor & d-tor */
	Downloader() noexcept;
	~Downloader();

	/* Move c-tor & assignment op */
	Downloader(Downloader&& other) noexcept;
	Downloader& operator=(Downloader&& other) noexcept;

	/* No copying */
	Downloader(const Downloader&) = delete;
	Downloader& operator=(const Downloader&) = delete;

	/** Download a web-page into the specified string */
	bool download(const char* url, std::string& output) const;

	/* Check is the object is properly initialized */
	inline bool isInit() const noexcept {return curl ? true : false;}

private:
	CURL* curl;
};



#endif // GG_DICTWOTD_DOWNLOADER_HPP
