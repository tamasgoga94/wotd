
# Sources for Linux machines with with the Gnome desktop environment
GNOME_LINUX_SRC = main.cpp downloader.cpp os/linux/os_linux.cpp

COMPILER = clang++-3.8 -std=c++1y -O2
WARNINGS = -Wall -Wextra -Winline -Werror

NAME = wotd

linux: $(GNOME_LINUX_SRC)
	$(COMPILER) $(WARNINGS) -o $(NAME) $(GNOME_LINUX_SRC) -lcurl


clean:
	rm $(NAME)
