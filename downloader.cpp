#include "downloader.hpp"

#include <curl/curl.h>


/* Used by cURL to download a web-site's contents into a string. */
static size_t writeToString(void* ptr, size_t size, size_t count, std::string* userdata) {
	auto oldSize = userdata->size();
    userdata->append(reinterpret_cast<char*>(ptr), size*count);
    return userdata->size() - oldSize;
}


/* c-tor: initializes a new CURL session */
Downloader::Downloader() noexcept
	: curl(curl_easy_init())
{;}


/* move c-tor */
Downloader::Downloader(Downloader&& other) noexcept {
	curl = other.curl;
	other.curl = nullptr;
}


/* move assignment op */
Downloader& Downloader::operator=(Downloader&& other) noexcept {
	curl_easy_cleanup(curl);
	curl = other.curl;
	other.curl = nullptr;
	return *this;
}


/* d-tor */
Downloader::~Downloader() {
	curl_easy_cleanup(curl);
}


/** download a web-page into the specified string */
bool Downloader::download(const char* url, std::string& output) const {
	if (curl == nullptr)
		return false;

	output.clear();

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeToString);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &output);
	CURLcode result = curl_easy_perform(curl);

	return result == CURLE_OK ? true : false;
}
